class NFLoader {

    constructor() {
        this.settings = {};
        this.params = '';
        this.buttonModal = document.querySelectorAll('.nf-button');
        this.widgetInject = document.querySelectorAll('.nf-widget');
    }

    get server() {
        return 'http://37.187.198.34/nf-hotels/server/';
    }

    set setSettings(p) {
        this.settings[p.key] = p.value;
    }

    set setParams(p) {
        this.params = p;
    }

    ctrl() {
        if (this.buttonModal.length > 0) {
            this.injectCss();
            this.buttonModalClick();
        }
        if (this.widgetInject.length > 0) {
            this.injectCss();
            this.wrapIframe();
            this.resizeParent();
            this.closeInnerPopups();
        }
    }

    clearSettings() {
        this.settings = {};
        this.params = '';
    }

    wrapIframe() {
        for (let w of this.widgetInject) {
            this.readSectionParams(w);
            w.innerHTML = this.iframeHtml();
        }
    }

    wrapModalIframe() {
        let body = document.querySelector('body');
        body.insertAdjacentHTML('beforeend', '<section class="nf-widget">' + this.iframeModalHtml() + '</section>');
        this.closeWidget();
    }

    iframeHtml() {
        let src = `${this.server}app-iframe.html#` + this.passUrlSettings() + this.passUrlParamsStright();
        return `<section class="nf-widget-inject"><iframe class="nfiframe" scrolling="no" src="${src}"></iframe></section>`;
    }

    iframeModalHtml() {
        let src = `${this.server}app-modal.html#` + this.passUrlSettings() + this.passUrlParams();
        return `<section class="nf-widget-modal"><iframe class="nfiframe" scrolling="yes" src="${src}"></iframe></section>`;
    }

    passUrlParams() {
        let url = '';
        if (this.params) {
            let p = this.params.replace(/&/g, '^');
            p = p.replace(/=/g, '!');
            url += !this.settings ? '?' : '';
            url += 'params=' + p;
        }
        return url;
    }

    passUrlSettings() {
        let url = ''
        if (this.settings) {
            url = '?';
            for (let key of Object.keys(this.settings)) {
                url += `${key}=` + this.settings[key] + '&'
            }
        }
        return url;
    }

    passUrlParamsStright() {
        let url = '';
        let p = location.search.replace('?', ''); + location.hash.replace('#', '');
        if (p) {
            url += !this.settings ? '?' : '';
            url += 'params=' + p;
        }
        return url;
    }

    buttonModalClick() {
        for (let b of this.buttonModal) {
            b.addEventListener('click', (event) => {
                this.readButtonParams(event);
                this.closeWidget();
                this.wrapModalIframe();
            });
        }
    }

    readButtonParams (event) {
        this.clearSettings();
        this.setSettings = {key: 'id', value: event.target.dataset.settingsId};
        this.setSettings = {key: 'slug', value: event.target.dataset.settingsSlug};
        this.setParams = event.target.dataset.params;
    }

    readSectionParams (element) {
        this.clearSettings();
        this.setSettings = {key: 'id', value: element.dataset.settingsId};
        this.setSettings = {key: 'slug', value: element.dataset.settingsSlug};
    }

    settingFromParent () {
        let out = {};
        let inp = location.hash.replace("#", "");
        inp = inp.replace("/?", "");
        inp.replace("?", "").split("&").map((part) => {
            let tmp = part.split("=");
            let key = tmp[0];
            let val = tmp[1];
            if (key == 'params') {
                let p = val.replace(/\!/g, '=');
                p = p.replace(/\^/g, '&');
                this.setParams = p;
            }
            else if ((typeof key == 'string') && (key.length > 0)) {
                out[key] = val;
                if (key == 'id' || key == 'slug') {
                    this.setSettings = {key: key, value: val};
                }
            }
        })
    }

    closeWidget() {
        window.onclick = (event) => {
            let widgetWrapper = document.querySelector('.nf-widget-modal');
            if (event.target == widgetWrapper) {
                widgetWrapper.remove();
            }
        }
    }

    injectCss() {
        let css = `
            .nf-widget {
                width: 100%;
            }
            .nf-widget .nf-widget-modal {
                position: fixed;
                z-index: 1;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                overflow: none;
                background-color: rgba(0,0,0,0.4);
                text-align: center;
            }
            .nf-widget .nf-widget-inject {
                position: relative;
                overflow: hidden;
                width: 100%;
            }
            .nf-widget .nf-widget-inject iframe {
                border: 0;
                width: 100%;
                min-height: 500px !important;
                z-index: 99999;
                top: 0;
                left: 0;
            }
            .nf-widget .nf-widget-modal iframe {
                border: 0;
                width: 90%;
                min-height: 500px !important;
                z-index: 99999;
                margin: auto;
            }
            `;

          let style = document.createElement('style');
          style.type = 'text/css';
          style.appendChild(document.createTextNode(css));
          document.querySelector('head').appendChild(style);
    }

    version() {
        var widgetLng = localStorage.getItem('nfWidgetLng');
        var version = 'pl';
        if (widgetLng) {
            version = widgetLng.split('|')[0];
        }
        this.prefix = this.server + 'dist/' + version + '/';
        this.rnd = 8;
    }

    bundles() {
        this.loadScripts('inline.bundle.js');
        this.loadScripts('polyfills.bundle.js');
        this.loadCss('styles.bundle.css');
    }

    loadBundles() {
        this.version();
        this.injectCss();
        this.bundles();
        this.loadMainBundle();
    }

    loadMainBundle() {
        this.loadScripts('main.bundle.js');
    }

    loadScripts(filename) {
        var fileref = document.createElement('script')
        fileref.setAttribute("type", "text/javascript")
        fileref.setAttribute("src", this.prefix + filename + '?v=' + this.rnd)
        document.getElementsByTagName("head")[0].appendChild(fileref)
    }

    loadCss(filename) {
        var fileref = document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", this.prefix + filename + '?v=' + this.rnd)
        document.getElementsByTagName("head")[0].appendChild(fileref)
    }

    resizeChild() {
        let dataParent = {top: 0, left: 0};
        window.onload = () => {
            let $widget = document.querySelector('article.widget-app');
            let widgetSize = $widget.getBoundingClientRect();
            let width = '100%';
            setInterval(() => {
                let heightInt = $widget.offsetHeight;
                let $modalInt  = document.querySelector('.cdk-global-overlay-wrapper');
                parent.postMessage({height: heightInt}, "*");
                if ($modalInt) {
                    parent.postMessage({modal: true}, "*");
                    let top = dataParent['top'];
                    let left = dataParent['left'];
                    width = dataParent['width'] + 'px';
                    $widget.style.position = 'absolute';
                    $widget.style.top = `${top}px`;
                    $widget.style.left = `${left}px`;
                    $widget.style.width = width;
                    $widget.style.height = `${height}px`;
                } else {
                    parent.postMessage({modal: false}, "*");
                    $widget.style.position = 'initial';
                    $widget.style.top = '0';
                    $widget.style.left = '0';
                    $widget.style.width = 'auto';
                    $widget.style.height = 'auto';
                }
            }, 20);
        };
        window.addEventListener('message', (event) => {
            dataParent = event.data;
            if (event.data['click']) {
                let $cdk = document.querySelectorAll('.cdk-overlay-container .cdk-overlay-pane');
                for(let $c of $cdk) {
                    $c.innerHTML = '';
                }
            }
        });
    }

    resizeParent() {
        let $iframe = document.querySelector('iframe.nfiframe');
        let initialWidgetWidth = this.widgetInject.offsetWidth;

        window.addEventListener('message', (event) => {
            let $body = document.querySelector('body');
            if (event.data['height']) {
                $iframe.style.height = event.data.height + 'px';
            }
            if (event.data['modal']) {
                $iframe.style.position = 'fixed';
                $iframe.style.top = '0';
                $iframe.style.left = '0';
                $body.style.overflow = 'hidden';
                this.widgetInject.style.width = `${initialWidgetWidth}px`;
            } else if (event.data['modal'] === false) {
                $iframe.style.position = 'initial';
                $body.style.overflow = 'initial';
            }
        });

        let prepereMsg = () => {
            let iframeSize = $iframe.getBoundingClientRect();
            let msg = {
                top: iframeSize.top + window.scrollY,
                left: iframeSize.left,
                width: initialWidgetWidth
            };
            if (msg.top != 0 && msg.left != 0) {
                $iframe.contentWindow.postMessage(msg, '*');
            }
        }

        setInterval(() => {
            initialWidgetWidth = this.widgetInject.offsetWidth;
            prepereMsg();
        }, 50);
    };

    closeInnerPopups() {
        let $iframe = document.querySelector('iframe.nfiframe');
        window.addEventListener('click', (event) => {
            $iframe.contentWindow.postMessage({click: true}, '*');
        });
    }

}

var NFL = new NFLoader();
NFL.ctrl();
