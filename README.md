##Jak osadzić widget jako okienko modal?
Przypisz `class="nf-button"` do dowolnego znacznika html, po wciśnięciu którego ma się pojawić okienko z widgetem.
Parametry do widgetu przekazywane są przez atrybut `data + nazwa parametru`.

###Przykład
`<button class="nf-button" data-settings-id="40" data-settings-slug="demo">Rezerwuj 1</button>`

##Jak osadzić widget na stronie?
Przypisz `class="nf-widget"` do elementu na stronie, w którym ma się wyświetlić widget.
Parametry do widgetu przekazywane są przez atrybut `data + nazwa parametru`.
Widget w tym przypadku osadzany jest jako iframe.
Komunikacja między aplikacją w iframe a stroną, na której osadzony jest widget odbywa się dzięki postMessage.
https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage

###Przykład
`<section class="nf-widget" data-settings-id="40" data-settings-slug="demo"></section>`

W każdym z przypadków należy zaincludować plik loader.js z docelowej lokalizacji na serwerze.

## Source

###Przykład osadzenia widgetu po stronie klienta
`http://37.187.198.34/nf-hotels/client/test.html`

###Struktura plików na serwerze:
    .
    
    ├── loader.js
    ├── app-iframe.html
    ├── app-modal.html
    ├── dist
        ├── pl
            ├── main.bundle.js        
            ├── inline.bundle.js
            ├── polyfills.bundle.js
            ├── styles.bundle.css
        ├── en
        ├── de
        ├── ru
    ├── assets
        ├── fonts
        ├── img
        ├── style




`loader.js`        - plik loadera

`app-modal.html`   - plik z kodem aplikacji, gdy widget ładowany jest jako modal

`app-iframe.html`  - plik z kodem aplikacji, gdy widget ładowany jest jako iframe

W katalogu `/server` znajdują się pliki, które trzeba umieścić na serwerze.

W `/server/dist/en` - znajduje się build dla wersji angielskiej, w `/server/dist/pl` dla wersji polskiej, itd.

W katalogu `/client` znajduje się testowe osadzenie widgetu.

W `loader.js` w metodzie `get server()` podaj adres, gdzie znajduje się loader.js na serwerze.